$(document).ready(function() {
	init();
});

function init(){
	product();
	addproduct();
	facebook();
	getProducts();
	editDelete();	

};
var num= $('.table tr').length;
var num = num-2;

// PRODUCT BOXES
function product(){
	$('button[name="add"]').on('click', function (){
		name = $(this).closest('.box').find('h3').text();
		price = $(this).closest('.box').find('span').text();
		quantity = $(this).closest('.box').find('input').val();
		$('.table tr:last').after('<tr><td><input name="product[]" class="lg-box" value="'+name+'"/></td><td><input type="text" name="price[]" class="sm-box" value="'+price+'"/><span class="coin">€</span></td><td><input name="quantity[]" class="sm-box" value="'+quantity+'"/></td><td></td>/tr>');
		num++;
		//console.log(num);
		if(num == 5){
			actions();						
		}
	});			
}

// LIST OF PRODUCTS
function addproduct(){
	$('input[name="more"]').on('click', function(){
		name= $(this).closest('tr').find('td').find('input').eq(0).val();
		price = $(this).closest('tr').find('td').find('input').eq(1).val();
		quantity = $(this).closest('tr').find('td').find('input').eq(2).val();

		$('.table tr:last').after('<tr><td><input name="product[]" class="lg-box" value="'+name+'"/></td><td><input type="text" name="price[]" class="sm-box" value="'+price+'"/><span class="coin">€</span></td><td><input name="quantity[]" class="sm-box" value="'+quantity+'"/></td><td></td></tr>');
		
		/*  clear first input */
		$(this).closest('tr').find('td').find('input').eq(0).val('');
		$(this).closest('tr').find('td').find('input').eq(1).val('');
		$(this).closest('tr').find('td').find('input').eq(2).val('');

			num++;
			//console.log(num);
			if(num == 5){				
				actions();		
			}	
	});
	// Añdir más productos al hacer click en el '+' en el perfil de la pareja
	$('input[name="moreproducts"]').on('click', function(){
		name= $(this).closest('tr').find('td').find('input').eq(0).val();
		price = $(this).closest('tr').find('td').find('input').eq(1).val();
		quantity = $(this).closest('tr').find('td').find('input').eq(2).val();

		$('.table tr:last').before('<tr><td><input name="product[]" class="lg-box" value="'+name+'"/></td><td><input type="text" name="price[]" class="sm-box" value="'+price+'"/><span class="coin">€</span></td><td><input name="quantity[]" class="sm-box" value="'+quantity+'"/></td><td></td></tr>');

		/*  clear first input */
		$(this).closest('tr').find('td').find('input').eq(0).val('');
		$(this).closest('tr').find('td').find('input').eq(1).val('');
		$(this).closest('tr').find('td').find('input').eq(2).val('');

	});
}
// SHOW FACEBOOK Y DISABLE BUTTONS
function actions(){
	setProducts();
	$('.social').show();
	$('input[name="more"]').closest('tr').hide();
	$('button[name="add"]').prop( "disabled", true );
}

/** FACEBOOK **/
function facebook(){

	$('#popupfb').on('click',function(event){
		event.preventDefault();
		var mywindow = window.open(
				"fb_config.php?provider=facebook", 
				"Facebook", 
				"width=500, height=300"
			);				
	});
}

//	Guardar los productos que han sido agregados en la pagina principal
function setProducts(){

	var n = $('table tr').length;
	num = n-1;
	products = new Array();
	$('table tr:gt(0)').each(function() {
		value = $(this).find('td input[name*=product]').val();
		value2 = $(this).find('td input[name*=price]').val();
		value3 = $(this).find('td input[name*=quantity]').val()+'|';
		//console.log(value);
		products.push(value);
		products.push(value2);
		products.push(value3);
	});
	console.log(products);

	localStorage.setItem('product' ,products);
}

//	Ver los productor guardados en el locastorage
function getProducts(){
	var values = localStorage.getItem('product');

	if(values !== null ){
		//$('#test').html(values);
			listProducts();
			$('.social').show();
			$('#popupfb').html('<img src="images/fb.png" class="facebook"/> Log Out');
			$('#popupfb').attr('href', 'fb_logout.php');
			$('input[name="more"]').closest('tr').hide();
			$('button[name="add"]').prop( "disabled", true );
	}
	localStorage.clear();
}

//	Listado de productos guardados en locastorage que se muestran en el perfil
function listProducts(){
	var values = localStorage.getItem('product');
	var row = values.split('|');
	
	v1 = row[0].split(',');  
	v2 = row[1].split(',');
	v3 = row[2].split(',');
	v4 = row[3].split(',');
	v5 = row[4].split(',');

	$('.table tr:first').after('<tr>'+
			'<td><input name="product[]" class="lg-box" value="'+v1[0]+'"/></td><td><input type="text" name="price[]" class="sm-box" value="'+v1[1]+'"/><span class="coin">€</span></td><td><input name="quantity[]" class="sm-box" value="'+v1[2]+'"/></td><td></td>></tr>'+
			'<tr><td><input name="product[]" class="lg-box" value="'+v2[1]+'"/></td><td><input type="text" name="price[]" class="sm-box" value="'+v2[2]+'"/><span class="coin">€</span></td><td><input name="quantity[]" class="sm-box" value="'+v2[3]+'"/></td><td></td>></tr>'+
			'<tr><td><input name="product[]" class="lg-box" value="'+v3[1]+'"/></td><td><input type="text" name="price[]" class="sm-box" value="'+v3[2]+'"/><span class="coin">€</span></td><td><input name="quantity[]" class="sm-box" value="'+v3[3]+'"/></td><td></td>></tr>'+
			'<tr><td><input name="product[]" class="lg-box" value="'+v4[1]+'"/></td><td><input type="text" name="price[]" class="sm-box" value="'+v4[2]+'"/><span class="coin">€</span></td><td><input name="quantity[]" class="sm-box" value="'+v4[3]+'"/></td><td></td>></tr>'+
			'<tr><td><input name="product[]" class="lg-box" value="'+v5[1]+'"/></td><td><input type="text" name="price[]" class="sm-box" value="'+v5[2]+'"/><span class="coin">€</span></td><td><input name="quantity[]" class="sm-box" value="'+v5[3]+'"/></td><td></td>></tr>');
}

if($('.social').val() == 'Log out'){
	console.log('aaaaaaaaaa');
}

// 	contar rows
var rowCount = $('.table tr').length;
console.log(rowCount);

num = (rowCount-2);
console.log(num);

// Agregar border-bottom. Solo es como guía para verificar desde donde agrega
lastrow = $('.table tr:eq('+num+') ').addClass('borderTr');

function editDelete(){
		$('input[name="edit"]').on('click', function(){
			row = $(this).parent().parent();
			console.log(row.html());

			$.ajax({
		      type: 'POST',
		      url: 'functions.php',
		      dataType: 'json',
		      data: row+'&action=edit',
		      success: function(response){
		          //console.log(response);
		          //$('#content').html(response.output);
		          location.reload();
		          actions();
		      },
		      error: function(data, textStatus, jqXHR){
		          console.log(data);
		          $('#content').html('<p>Error editar data</p>');
		      }

		  });
		});
		$('input[name="delete"]').on('click', function(){
			row = $(this).parent().parent();
			
			$.ajax({
		      type: 'POST',
		      url: 'functions.php',
		      dataType: 'json',
		      data: row+'&action=delete',
		      success: function(response){
		          //console.log(response);
		          //$('#content').html(response.output);
		          location.reload();
		          actions();
		      },
		      error: function(data, textStatus, jqXHR){
		          console.log(data);
		          $('#content').html('<p>Error editar data</p>');
		      }

		  });
		});
}


/*
lastrow = $('.table tr:eq('+num+') td:lt(3) ').find('input').each(function() {
    console.log($(this).val());
  });
*/



