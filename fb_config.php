<?php
session_start();
   $config = dirname(__FILE__) . '/fb.php';
   require_once( "Hybrid/Auth.php" );

   try{
       $hybridauth = new Hybrid_Auth( $config );
      // Se conecta a facebook
       $facebook = $hybridauth->authenticate( "Facebook" );

       // Si es correcto abre una sesion
       if($facebook){
        $_SESSION['user_identifier'] = $facebook->getUserProfile()->identifier;
        
        echo '<script type="text/javascript"> 
                  window.opener.location.reload(true);
                  window.close();
              </script>';

       } 
        //call to get stored data
       // Hybrid_Auth::getSessionData();

       // get the user profile
    $facebook_user_profile = $facebook->getUserProfile();
 
    echo "As: <b>{$facebook_user_profile->displayName}</b><br />";
    echo "And your provider user identifier is: <b>{$facebook_user_profile->identifier}</b><br />";
  
 
    // debug the user profile
    //print_r( $facebook_user_profile );
   
    //header('Location: index.php?id='.$_SESSION['user_identifier']);
    // exp of using the facebook social api: Returns settings for the authenticating user.
    //$account_settings = $facebook->api()->get( 'account/settings.json' );
 
    // print recived settings
    //echo "Your account settings on facebook: " . print_r( $account_settings, true );
 
    // disconnect the user ONLY form facebook
    // this will not disconnect the user from others providers if any used nor from your application
    echo "Logging out..";
    $facebook->logout();
    //print_r($_SESSION['user_identifier']); 
   }
    catch( Exception $e ){
    // Display the recived error,
    // to know more please refer to Exceptions handling section on the userguide
    switch( $e->getCode() ){
      case 0 : echo "Unspecified error."; break;
      case 1 : echo "Hybriauth configuration error."; break;
      case 2 : echo "Provider not properly configured."; break;
      case 3 : echo "Unknown or disabled provider."; break;
      case 4 : echo "Missing provider application credentials."; break;
      case 5 : echo "Authentification failed. "
                  . "The user has canceled the authentication or the provider refused the connection.";
               break;
      case 6 : echo "User profile request failed. Most likely the user is not connected "
                  . "to the provider and he should authenticate again.";
               $facebook->logout();
               break;
      case 7 : echo "User not connected to the provider.";
               $facebook->logout();
               break;
      case 8 : echo "Provider does not support this feature."; break;
    }
 
    // well, basically your should not display this to the end user, just give him a hint and move on..
    echo "<br /><br /><b>Original error message:</b> " . $e->getMessage();
  }


