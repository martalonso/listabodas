<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Lista de Bodas</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div class="container">		
		<h1>TITULAR</h1>
		<h2>Subtitular</h2>

	<section class="row">
			<p>Elige tus productos*</p>
			<ul class="boxes">
				<li class="box">
					<h3>Cookie.</h3>
					<p> Precio: <span name="productprice" class="productprice"/>25</span> €</p>
					<p> Cantidad: <input type="text" name="cont" class="sm-box"/></p>
					<button type="button" name="add">Añadir</button>
				</li>
				<li class="box">
					<h3>Tart.</h3>
					<p> Precio: <span name="productprice" class="productprice"/>150</span> €</p>
					<p> Cantidad: <input type="text" name="cont" class="sm-box"/></p>
					<button type="button" name="add">Añadir</button>
				</li>
				<li class="box">
					<h3>Gummi bears.</h3>
					<p> Precio: <span name="productprice" class="productprice"/>35.5</span> €</p>
					<p> Cantidad: <input type="text" name="cont" class="sm-box"/></p>
					<button type="button" name="add">Añadir</button>
				</li>
				<li class="box">
					<h3>Cookie tart.</h3>
					<p> Precio: <span name="productprice" class="productprice"/>80</span> €</p>
					<p> Cantidad: <input type="text" name="cont" class="sm-box"/></p>
					<button type="button" name="add">Añadir</button>
				</li>
				<li class="box">
					<h3>Gummi .</h3>
					<p> Precio: <span name="productprice" class="productprice"/>10</span> €</p>
					<p> Cantidad: <input type="text" name="cont" class="sm-box"/></p>
					<button type="button" name="add">Añadir</button>
				</li>
				<li class="box">
					<h3>Cookie bears.</h3>
					<p> Precio: <span name="productprice" class="productprice"/>53.7</span> €</p>
					<p> Cantidad: <input type="text" name="cont" class="sm-box"/></p>
					<button type="button" name="add">Añadir</button>
				</li>
			</ul>
			<p>* Tienes que añadir un mínimo de 5 para poder registrarlos</p>
	</section>
	<form method="POST" action="functions.php">
	<section class="row">
		<p>... o añade lo que quieras</p>
		<table class="table">
			<tr>
				<td>Nombre</td>
				<td>Precio</td>
				<td>Cantidad</td>
				<td>Añadir</td>
			</tr>			
			<tr>
				<td><input type="text" name="product[]" class="lg-box"/> </td>
				<td><input type="number" name="price[]" class="sm-box"/><span class="coin">€</span></td>
				<td><input type="number" name="quantity[]" class="sm-box"/></td>
				<td><input type="button" name="more" class="sm-box" value="+"/></td>
			</tr>
		</table>
	</section>

	<section class="row data">	
		<p class="social">Registrate con 
     <a href="" id="popupfb"><img src="images/fb.png" class='facebook'/> Login</a>
    </p>
			<?php
				if(isset($_SESSION['user_identifier']) ) {
					//var_dump( $_SESSION['user_identifier'] );	
		 	?>
				<p class="couple">
					<span class="text">Nombre de los novios </span>
					<input type="text" name="groom" class="md-box" placeholder="Juan" required/> y <input type="text" name="bride" class="md-box" placeholder="María" required/>
				</p>
				<p class="account">
					<span class="text">Cuenta bancaria: </span>
					<input type="text" name="account" class="lg-box" value=" ES1020903200500041045040" required/>
				</p>
				<div id='test'></div>
		<?php } ?>
			<button type="submit" name="next" class="btn">Siguiente</button>
		</form>
	</section>
	
	</div> <!-- END CONTAINER -->
	
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 <script src="script/script.js"></script>
</html>	

