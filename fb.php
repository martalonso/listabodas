<?php
// Configuración de Facebook
return
  array(
    "base_url" => "http://localhost/listabodas/hybrid.php",

    "providers" => array (
      // openid providers
      "OpenID" => array (
        "enabled" => true
      ),
      "Facebook" => array (
        "enabled" => true,
         "keys"    => array ( "id" => "833221746755673", "secret" => "47a60755f6c3f19379101d946fc2d110" ),
        "trustForwarded" => false,
         "display" => "popup"

      ),
    ),

    // If you want to enable logging, set 'debug_mode' to true.
    // You can also set it to
    // - "error" To log only error messages. Useful in production
    // - "info" To log info and error messages (ignore debug messages)
    "debug_mode" => false,

    // Path to file writable by the web server. Required if 'debug_mode' is not false
    "debug_file" => "",
  );


